$(document).ready(function() {

    $(".bigger").click(function() {

        $(this).height($(this).height() + 200);
    });

    new docHeightChange().init().add(function() {

        $(".console").append($("<p />", {text: "Height change detected!"}));
    })
});