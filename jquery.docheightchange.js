/**
 * jquery.docheightchange
 * Version: 0.9
 * Last Updated: February 19, 2013
 * Requires: jQuery v1.8.3
 *
 * Copywrite: (c) 2012 EYEMAGINE Technology, LLC
 * Website: http://www.eyemaginetech.com
 *
 * Licences: MIT, GPL
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 */
/*******************************************************************************

Example usage:

    var heightChange = new docHeightChange();

    // In document ready
    heightChange.init();
    
    // Add the function we want to be called when the height changes
    // .add method supports anonymous functions or defined [ie .add(someFunc)]
    heightChange.add(function() {
        
        alert('the document height just changed');
    });


You may also chain the methods together:

    new docHeightChange().init().add(someFunc).add(someOtherFunc);

*******************************************************************************/
(function($) {

    docHeightChange.prototype = {
        /**
         * Adds the given callback to the collection of callbacks
         *
         * @param function callback
         *
         * @return docHeightChange
         */
        add: function(callback) {

            this.callbacks.push(callback);

            return this;
        },
        /**
         * Saves the initial value of the document height to check against
         * and starts the recursive checker
         *
         * @return docHeightChange
         */
        init: function() {

            this.setHeight();
            this.timeChecker();

            return this;
        },
        /**
         * Calls all callback functions stored in the collection
         *
         * @return docHeightChange
         */
        runCallbacks: function() {

            for (var i = 0; i < this.callbacks.length; i++) this.callbacks[i]();

            return this.setHeight();
        },
        /**
         * Sets the stored document height to check against
         *
         * @return docHeightChange
         */
        setHeight: function() {            

            this.height = $(document).height();

            return this;
        },
        /**
         * Recursively checks for a height change at the given interval.
         * If a change is detected, it will run the callbacks and store the
         * new document height.
         *
         * @return void
         */
        timeChecker: function() {

            var that = this;

            if ($(document).height() != this.height) this.runCallbacks();

            setTimeout(function() {

                that.timeChecker();
            }, that.interval);
        }
    };
})(jQuery);
/**
 * Detects document height change and runs callback functions
 * 
 * @param int checkInterval How often to recursively check for a height change
 *                          in millisecond (default 25)
 *
 * @return docHeightChange
 */
function docHeightChange(checkInterval) {

    this.height;
    this.interval = checkInterval || 25;
    this.callbacks = new Array();
}