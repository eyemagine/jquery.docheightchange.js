jquery.docheightchange.js
=========================

jquery.docheightchange
Version: 0.9
Last Updated: February 19, 2013
Requires: jQuery v1.8.3

Copywrite: (c) 2012 EYEMAGINE Technology, LLC
Website: http://www.eyemaginetech.com

Licences: MIT, GPL
http://www.opensource.org/licenses/mit-license.php
http://www.gnu.org/licenses/gpl.html

# Example usage:

    var heightChange = new docHeightChange();

    // In document ready
    heightChange.init();
    
    // Add the function we want to be called when the height changes
    // .add method supports anonymous functions or defined [ie .add(someFunc)]
    heightChange.add(function() {
        
        alert('the document height just changed');
    });


# You may also chain the methods together:

    new docHeightChange().init().add(someFunc).add(someOtherFunc);